all: doc/Notes.pdf reports/test_invitro.pdf reports/test_invivo.pdf

PANDOC_ARGS=--from markdown+autolink_bare_uris --filter pandoc-citeproc

# General compilation recipe for Markdown docs
doc/%.pdf: doc/%.md
	pandoc $(PANDOC_ARGS) -o $@ $<

# General compilation recipe for reports
reports/%.pdf: src/%.Rmd
	Rscript -e 'rmarkdown::render("$<", output_dir = "reports", knit_root_dir = "..")'

# If data or model results change, mark the corresponding source as updated
src/test_invivo.Rmd: data/deriv/lumin.rds cache/lumin_fit.rds
	touch $@

# Model results
cache/lumin_fit.rds: src/fit_sm.R src/sm.stan data/deriv/lumin.rds
	Rscript $<

# Derived datasets
data/deriv/lumin.rds: src/1_cleanup_invivo.R data/topo\ manip\ in\ vivo.xlsx
	Rscript $<

