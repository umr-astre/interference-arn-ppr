functions {
  /**
  * Generate a random sample of iid Gaussian observations.
  *
  * @param n Number of observations
  * @param mu Mean
  * @param sigma Standard Deviation
  * @return Return an n-vector of iid random normal variables.
  */

  vector rnorm_rng(int n, real mu, real sigma) {
    vector[n] x;
    for (k in 1:n) {
      if (sigma <= 0)
        x[k] = mu;
      else
        x[k] = normal_rng(mu, sigma);
    }
    return x;
  }

  /**
  * Generate a random sample of iid zero-truncated Normal observations.
  *
  * @param n Number of observations
  * @param mu Mean
  * @param sigma Standard Deviation
  * @return Return an n-vector of iid random Half-Cauchy variables.
  */

  vector rhn_rng(int n, real mu, real sigma) {
    vector[n] x = fabs(rnorm_rng(n, mu, sigma));
    return x;
  }

  /**
  * Generate a random sample of iid Half-Cauchy observations.
  *
  * @param n Number of observations
  * @param mu Mean
  * @param s Scale
  * @return Return an n-vector of iid random Half-Cauchy variables.
  */

  vector rhc_rng(int n, real mu, real s) {
    vector[n] x;
    for (k in 1:n)
      x[k] = fabs(cauchy_rng(mu, s));
    return x;
  }

  /**
  * Return draws from the hierarchical model with data matrix X with integer
  * values for the animal (i), day (j), leg (k), treatment (t) and source (s),
  * and parameters vector theta (the order is documented in the report).
  *
  * The variable leg is nested within animal which is in turn nested within
  * treatment, and they are coded accordingly (i.e. you need the three variables)
  * to identify a leg. Thus i varies in 1..4, k in 1..2 and t in 1..3.
  *
  * @param X Data matrix (N x 5)
  * @param theta Parameter vector (19 x 1)
  * @return Return an N-vector of draws from the model.
  */

  vector interference_rng(int[] animal, int[] day, int[] leg, int[] treatment, int[] source, vector theta) {
    // Number of animals, days, legs, treatment and sources are fixed to
    // reflect the experiment setup.
    // Nesting: treatment >> animal >> leg
    int n_it = 4;  // number of animals per treatment
    int n_j = 10;  // number of days
    int n_ki = 2;  // number of legs per animal
    int n_t = 3;  // number of treatments
    int n_s = 2;  // number of sources

    int n_i = n_it * n_t;  // total number of animals (12)
    int n_k = n_ki * n_i;  // total number of legs (24)

    // Non-varying effects
    real nuRN = theta[1];
    real nuIFF = theta[2];
    real alpha0_AVT = theta[3];
    real alpha0_DMA = theta[4];
    real beta0_AVT = theta[5];
    real beta0_DMA = theta[6];

    // Varying intercepts coefficients
    vector[n_i] nu_ARN = rnorm_rng(n_i, 0, theta[7]);
    vector[n_i] nu_AIFF = rnorm_rng(n_i, 0, theta[8]);
    vector[n_k] nu_LRN = rnorm_rng(n_k, 0, theta[9]);
    vector[n_k] nu_LIFF = rnorm_rng(n_k, 0, theta[10]);

    // Varying initial deviations for both concerned treatments
    vector[n_i] alpha_AVT_A = rhn_rng(n_i, 0, theta[11]);
    vector[n_i] alpha_DMA_A = rhn_rng(n_i, 0, theta[12]);
    vector[n_k] alpha_AVT_L = rhn_rng(n_k, 0, theta[13]);
    vector[n_k] alpha_DMA_L = rhn_rng(n_k, 0, theta[14]);

    // Varying decays for both concerned treatments
    vector[n_i] beta_AVT_A = rnorm_rng(n_i, 0, theta[15]);
    vector[n_i] beta_DMA_A = rnorm_rng(n_i, 0, theta[16]);
    vector[n_k] beta_AVT_L = rnorm_rng(n_k, 0, theta[17]);
    vector[n_k] beta_DMA_L = rnorm_rng(n_k, 0, theta[18]);

    // Varying paramters by source, leg within animal within treatment
    vector[n_k * n_s] mu;
    vector[n_k * n_j] f_AVT;
    vector[n_k * n_j] f_DMA;
    vector[n_k] alpha_AVT;
    vector[n_k] alpha_DMA;
    vector[n_k] phi_AVT;
    vector[n_k] phi_DMA;

    // Linear predictor
    vector[n_s * n_k * n_j] eta;

    // Residual standard deviation
    real sigma = theta[19];

    // Output vector as long as the number of rows in X
    int nobs = size(animal);  // number of rows in X
    vector[nobs] y;

    // Now fill it in
    for (s in 1:n_s) {  // 1..2
      for (t in 1:n_t) {  // 1..3
        for (it in 1:n_it) {  // 1..4
          for (ki in 1:n_ki) {  // 1..2
            // leg:     1, 2, 3, 4, 5, 6, 7, 8, 9, ..., 24
            // animal:  1, 1, 2, 2, 3, 3, 4, 4, 5, ..., 12
            // treatm:  1, 1, 1, 1, 1, 1, 1, 1, 2, ..., 3
            int i = (t-1)*n_it + it;  // animal idx 1..12
            int k = (i-1)*n_ki + ki;  // leg idx 1..24
            int iks = (s-1)*n_k + k; // index for leg in animal by source

            // Vaying intercepts at Renilla
            mu[iks] = nuRN + nu_ARN[i] + nu_LRN[k];
            if (s == 1)  // supplementary intercept at Firefly
              mu[iks] = mu[iks] + nuIFF + nu_AIFF[i] + nu_LIFF[k];

            // Varying initial departures
            alpha_AVT[k] = alpha0_AVT + alpha_AVT_A[i] + alpha_AVT_L[k];
            alpha_DMA[k] = alpha0_DMA + alpha_DMA_A[i] + alpha_DMA_L[k];

            // Varying decays
            phi_AVT[k] = exp(beta0_AVT + beta_AVT_A[i] + beta_AVT_L[k]);
            phi_DMA[k] = exp(beta0_DMA + beta_DMA_A[i] + beta_DMA_L[k]);

            for (j in 1:n_j) {
              int jk = (k-1)*n_j + j; // index for day in leg
              int jks = (s-1)*(n_j * n_k) + jk; // index for day in leg in source

              // Linear predictor
              eta[jks] = mu[iks];

              // Departure curves, conditional on treatment and source
              // "Multiply" by the indicator functions of the treatment
              if (s == 1) {  // Firefly
                f_AVT[jk] = alpha_AVT[k] * exp(-(j-1)/phi_AVT[k]);
                f_DMA[jk] = alpha_DMA[k] * exp(-(j-1)/phi_DMA[k]);
                if (t > 1) {
                  eta[jks] = eta[jks] - f_AVT[jk];
                }
                if (t > 2) {
                  eta[jks] = eta[jks] - f_DMA[jk];
                }
              }
            }
          }
        }
      }
    }

    // Observations
    for (o in 1:nobs) {
      int s = source[o];  // source (1..2)
      int i = (treatment[o]-1)*n_it + animal[o];  // animal (1..12)
      int k = (i-1)*n_ki + leg[o];  // leg (1..24)
      int j = day[o];  // day (1..10)

      // index in the linear predictor of size n_s * n_k * n_j
      int idx = (s-1)*(n_j * n_k) + (k-1)*n_j + j;
      y[o] = normal_rng(eta[idx], sigma);
    }

    return y;
  }
}
data {
  // If we were estimating a model, we'd define the data inputs here

  // unmodelled data
  int<lower = 0> N; // number of observations
  int n_it;         // number of animals per treatment
  int n_j;          // number of days
  int n_ki;         // number of legs per animal
  int n_t;          // number of treatments
  int n_s;          // number of sources
  int animal[N];    // animal id (within treatment) for each observation
  int day[N];       // day id for each observation
  int leg[N];       // leg id (within animal) for each observation
  int treatment[N]; // treatment id for each observation
  int source[N];    // source id for each observation

  // modelled data
  vector[N] y;      //outcome vector
}
transformed data {
  int n_i = n_it * n_t;  // total number of animals
  int n_k = n_ki * n_i;  // total number of legs
}
parameters {
  // ... and the parameters we want to estimate would go in here
  vector[n_s] nu;               // global intercept at RN and difference for FF
  real<lower = 0> alpha0_AVT;   // initial departure for AVT
  real<lower = 0> alpha0_DMA;   // supplementary initial departure for DMA
  real beta0_AVT;               // decay rate for AVT
  real beta0_DMA;               // supplementary decay rate for DMA
  real<lower = 0> s_nu[4];      // sigma nu_ARN, nu_AIFF, nu_LRN, nu_LIFF
  real<lower = 0> s_alpha[4];   // sds for alpha AVT_A, DMA_A, AVT_L, DMA_L
  real<lower = 0> s_beta[4];    // sds for beta AVT_A, DMA_A, AVT_L, DMA_L
  real<lower = 0> sigma;        // residual sd

  // Varying intercepts coefficients
  vector[n_i] nu_ARN_tilde;
  vector[n_i] nu_AIFF_tilde;
  vector[n_k] nu_LRN_tilde;
  vector[n_k] nu_LIFF_tilde;

  // Varying initial deviations for both concerned treatments
  vector<lower = 0>[n_i] alpha_AVT_A_tilde;
  vector<lower = 0>[n_i] alpha_DMA_A_tilde;
  vector<lower = 0>[n_k] alpha_AVT_L_tilde;
  vector<lower = 0>[n_k] alpha_DMA_L_tilde;

  // Varying decays for both concerned treatments
  vector[n_i] beta_AVT_A_tilde;
  vector[n_i] beta_DMA_A_tilde;
  vector[n_k] beta_AVT_L_tilde;
  vector[n_k] beta_DMA_L_tilde;
}
transformed parameters {
  vector[N] obs_eta;  // linear predictor in the observed data
  vector[n_s * n_k * n_j] eta;  // linear predictor by source, leg(an(tr)) and day
  vector[n_s * n_k] mu;  // intercept by source and leg(an(tr))
  vector[n_k * n_j] f_AVT;
  vector[n_k * n_j] f_DMA;
  vector[n_k] alpha_AVT;
  vector[n_k] alpha_DMA;
  vector[n_k] phi_AVT;
  vector[n_k] phi_DMA;

  // Scaled varying coefficients
  vector[n_i] nu_ARN;
  vector[n_i] nu_AIFF;
  vector[n_k] nu_LRN;
  vector[n_k] nu_LIFF;

  vector[n_i] alpha_AVT_A;
  vector[n_i] alpha_DMA_A;
  vector[n_k] alpha_AVT_L;
  vector[n_k] alpha_DMA_L;

  vector[n_i] beta_AVT_A;
  vector[n_i] beta_DMA_A;
  vector[n_k] beta_AVT_L;
  vector[n_k] beta_DMA_L;

  nu_ARN = s_nu[1] * nu_ARN_tilde;
  nu_AIFF = s_nu[2] * nu_AIFF_tilde;
  nu_LRN = s_nu[3] * nu_LRN_tilde;
  nu_LIFF = s_nu[4] * nu_LIFF_tilde;

  alpha_AVT_A = s_alpha[1] * alpha_AVT_A_tilde;
  alpha_DMA_A = s_alpha[2] * alpha_DMA_A_tilde;
  alpha_AVT_L = s_alpha[3] * alpha_AVT_L_tilde;
  alpha_DMA_L = s_alpha[4] * alpha_DMA_L_tilde;

  beta_AVT_A = s_beta[1] * beta_AVT_A_tilde;
  beta_DMA_A = s_beta[2] * beta_DMA_A_tilde;
  beta_AVT_L = s_beta[3] * beta_AVT_L_tilde;
  beta_DMA_L = s_beta[4] * beta_DMA_L_tilde;

  // Derived quantities: mu, alpha_t, phi_t, f_t, eta, obs_eta
  for (s in 1:n_s) {  // 1..2
    for (t in 1:n_t) {  // 1..3
      for (it in 1:n_it) {  // 1..4
        for (ki in 1:n_ki) {  // 1..2
          // leg:     1, 2, 3, 4, 5, 6, 7, 8, 9, ..., 24
          // animal:  1, 1, 2, 2, 3, 3, 4, 4, 5, ..., 12
          // treatm:  1, 1, 1, 1, 1, 1, 1, 1, 2, ..., 3
          int i = (t-1)*n_it + it;  // animal idx 1..12
          int k = (i-1)*n_ki + ki;  // leg idx 1..24
          int iks = (s-1)*n_k + k; // index for leg in animal by source
          int is = (s-1)*n_i + i; // index for animal by source

          // Vaying intercepts
          mu[iks] = nu[1] + nu_ARN[i] + nu_LRN[k];
          if (s == 1)  // Interaction with source FF: I{FF}
            mu[iks] = mu[iks] + nu[2] + nu_AIFF[i] + nu_LIFF[k];

          // Varying initial departures
          alpha_AVT[k] = alpha0_AVT + alpha_AVT_A[i] + alpha_AVT_L[k];
          alpha_DMA[k] = alpha0_DMA + alpha_DMA_A[i] + alpha_DMA_L[k];

          // Varying decays
          phi_AVT[k] = exp(beta0_AVT + beta_AVT_A[i] + beta_AVT_L[k]);
          phi_DMA[k] = exp(beta0_DMA + beta_DMA_A[i] + beta_DMA_L[k]);

          for (j in 1:n_j) {
            int jk = (k-1)*n_j + j; // index for day in leg
            int jks = (s-1)*(n_j * n_k) + jk; // index for day in leg

            // Linear predictor
            eta[jks] = mu[iks];

            // Departure curves, conditional on treatment and source
            // "Multiply" by the indicator functions of the treatment
            // I can't have 240 f effects since for the first 4 animals I
            // won't have anything defined. Otherwise, I need to fill it with
            // t = 1 as well.
            if (s == 1) {  // Firefly
              //
              f_AVT[jk] = alpha_AVT[k] * exp(-(j-1)/phi_AVT[k]);
              f_DMA[jk] = alpha_DMA[k] * exp(-(j-1)/phi_DMA[k]);
              if (t > 1) {
                eta[jks] = eta[jks] - f_AVT[jk];
              }
              if (t > 2) {
                eta[jks] = eta[jks] - f_DMA[jk];
              }
            }

          }
        }
      }
    }
  }

  // Subset observed eta
  for (o in 1:N) {
    int s = source[o];  // source (1..2)
    int i = (treatment[o]-1)*n_it + animal[o];  // animal (1..12)
    int k = (i-1)*n_ki + leg[o];  // leg (1..24)
    int j = day[o];  // day (1..10)

    // index in the linear predictor of size n_s * n_k * n_j
    int idx = (s-1)*(n_j * n_k) + (k-1)*n_j + j;
    obs_eta[o] = eta[idx];
  }

}
model {
  // This is where the probability model we want to estimate would go

  // Define the priors
  // I coud vectorise the parameters sharing the same prior, and assign
  // the prior to the whole vector in one line
  // Furthermore, I could define the prior parameters in the data block
  // to allow varying the priors from the outside.
  nu ~ normal(10, 10);

  alpha0_AVT ~ normal(0, 8.6);
  alpha0_DMA ~ normal(0, 8.6);

  beta0_AVT ~ normal(0, 0.4);
  beta0_DMA ~ normal(0, 0.4);

  s_nu ~ cauchy(0, 1.58);  // s_nuARN, s_nuAIFF, s_nuLRN, s_nuLIFF

  s_alpha ~ cauchy(0, 1.36);  // sigma_AVT_A, DMA_A, AVT_L, DMA_L

  s_beta ~ cauchy(0, 0.063);  // sigma_AVT_A, DMA_A, AVT_L, DMA_L

  sigma ~ cauchy(0, 0.31);

  // Intermediate quantities
  // Varying intercepts coefficients
  nu_ARN_tilde ~ normal(0, 1);
  nu_AIFF_tilde ~ normal(0, 1);
  nu_LRN_tilde ~ normal(0, 1);
  nu_LIFF_tilde ~ normal(0, 1);

  // Varying initial deviations for both concerned treatments
  alpha_AVT_A_tilde ~ normal(0, 1);
  alpha_DMA_A_tilde ~ normal(0, 1);
  alpha_AVT_L_tilde ~ normal(0, 1);
  alpha_DMA_L_tilde ~ normal(0, 1);

  // Varying decays for both concerned treatments
  beta_AVT_A_tilde ~ normal(0, 1);
  beta_DMA_A_tilde ~ normal(0, 1);
  beta_AVT_L_tilde ~ normal(0, 1);
  beta_DMA_L_tilde ~ normal(0, 1);


  // The likelihood
  y ~ normal(obs_eta, sigma);

}
generated quantities {
  // For model comparison, we'll want to keep the likelihood contribution of each point
  // We will also generate posterior predictive draws (yhat) for each data point. These will be elaborated on below.

  // TODO: simulate y_sim not only for the observed values, but for all points
  vector[N] log_lik;
  vector[N] y_sim;
  for(i in 1:N){
    log_lik[i] = normal_lpdf(y[i] | obs_eta[i], sigma);
    y_sim[i] = normal_rng(obs_eta[i], sigma);
  }
}
