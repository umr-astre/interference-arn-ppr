Interference ARN vaccine for PPR
================================

This repository contains the analyses of various clinical trials of vaccine development for PPR.


## Therapeutic vaccine

- [Exploratory data analysis](https://umr-astre.pages.mia.inra.fr/interference-arn-ppr/tna-descriptive.html)

- [Model for the viral concentration](https://umr-astre.pages.mia.inra.fr/interference-arn-ppr/tna-viral_concentration.html)

- [Model for clinical score](https://umr-astre.pages.mia.inra.fr/interference-arn-ppr/tna-clinical_score.html)

  - [Variation with individual effect for goat 12](https://umr-astre.pages.mia.inra.fr/interference-arn-ppr/tna-clinical_score_id12.html)


## _In vivo_ trial



## _In vitro_ trial




<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://forgemia.inra.fr/umr-astre/interference-arn-ppr">Interference-ARN-PPR code</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://forgemia.inra.fr/famuvie">Facundo Muñoz</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0<img style="height:22px!important;margin-left:5px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p> 
