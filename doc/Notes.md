---
title: Interference ARN for PPR (Peste des Petits Ruminants)
author: Facundo Muñoz
date: Nov, 2017
output:
  pdf_document:
  html_document: default
documentclass: cirad
---

# Interference ARN for PPR (Peste des Petits Ruminants)

__Vaccine development__

## Context



2020-12-11 R Servan

Sent by e-mail the data in tidy format: `Fichier_Results_Facundo_final.xlsx`


2020-11-25 R Servan

New study with the same treatment, but testing a different administration method.
We had previously the _in vitro_ which tested the treatment in a cellular culture and the
_in vivo_ which tested the treatment in a rat muscle. Neither of those accounted
for the administration procedure in real goats and how the treatment was transported
to infected cells.

__Therapeutic vaccine__ (i.e. to treat the disease, rather than prevent it).
Inserted the treatment in attenuated virus from classic PPR vaccine made resistant.
This virus will hopefully transport the treatment to infected cells with the real
(wild) virus.

### Data

- 25-30 individual animals (goats) split into 5 groups (boxes)

- Groups 1-3 (boxes 1,2,5): treatment groups with vaccine administered at days -1, 0, 1 with 
respect to the challenge (infection with real virus).

- Groups 4-5 (boxes 6,8): negative control groups, with classical PPR vaccine (not expected
to have any therapeutic effect) and no treatment at all.

- Response variables:

  - Clinical score (SC): categorical variable from 1 (no symptoms) to 5 (death)
  
  - Viral concentration : quantitative variable. Not clear how are we going to quantify it.
      We have measures of "Threshold cycle" (Ct) aka "Quantification cycle" (Cq)
      which quantifies the relative concentration of virus inversely: number of cycles
      necessary to find the virus DNA, running from 0 (immediately detected, thus
      high concentration to 40 cycles (not detected)). 
      Lower measures correspond to higher concentrations [^1].
      Otherwise, we can use a measured derived from Ct that directly estimates
      the viral charge (number of copies of viral genes). Which variable and
      potential transformations of them are to be determined.
      
      Asked Etienne Loire (see answer 2020-11-27). Essentially, the calibration
      is a linear transformation, so from a modelling perspective is equivalent.
      It can be transformed, though, so it has nicer statistical properties.
      
- Measurements:

  - Each animal is evaluated for both response variables almost every day from 5
  to 14. There are some missing values.
  

Explanation of the data in the file "Fichier_Results_Facundo.xlsx"

> Alors, dans le tableau excel, tu trouveras pour chaque échantillon, une valeur de Cq du gène cible (Cq PPR) et une valeur de Cq d'un gène de référence (Cq HKG). Le gène de référence est un gène cellulaire qui permet d’identifier les biais liés à l’échantillonage et l’extraction des l’acides nucléiques (ARN) pour la détection qPCR. Sachant que le virus recherché est à l’intérieur de la cellule, le gène de référence permet de normaliser l’échantillon afin de mesurer la charge viral dans le même nombre de cellules dans tous les échantillons (gommer les biais liés soit à l’échantillonage, soit à l’extraction d’acides nucléique).
> Par contre, je ne sais pas quelle est la meilleur manière de normaliser les Cq du gène cible en utilisant le gène de référence. 

### Proposed analyses

- Exploratory analysis by 2020-12-11 to decide continuation. Goal, visualise
the efficacy of the treatment.

- Modelling and estimation of treatment efficacy by 2021-02-12.


  
  
[^1] https://www.thermofisher.com/fr/fr/home/life-science/pcr/real-time-pcr/real-time-pcr-learning-center/real-time-pcr-basics/real-time-pcr-understanding-ct.html

2018-11-12 meeting with Renata

- Issue with previous in-vivo analysis: we assumed treatment DM+AVT would be at least as performant as AVT alone. However, this is incorrect since DM+AVT does not __contain__ AVT strictly. In fact, AVT alone is directly injected in the target cells by means of an electric shock (electroporé). This is known to work. On the other hand, DM+AVT relies on the delivery molecule to transport the treatment. This can work better but not necessarily. Thus, we need to relax this assumption, and consider AVT and DM+AVT as alternative treatments.

- New _Specificity Analysis_. See doc/essai\ 1\ specificite\ siRNA\ in\ vivo.pdf. This aims to verify that the mutation of a single nucleotide in either the siARN molecule or the plasmid causes the treatment to stop working. However if the mutation is introduced in both the molecule and the plasmid, then it works again.

		The dataset is in data/données\ brutes\ essai\ 1.xlsx. There, we have daily measurements as in the precedent analisis. Lots 1 and 2 correspond to non-mutated and mutated siRNA molecules. Left and Right legs correspond to mutated and non-mutated plasmid.


2017-10-04 meeting with Renata, Cécile and Vladimir.

They have a molecule of ARN that prevents the virus of the PPR to replicate.

They test ways of delivering the molecule.


## Proposed Analyses


### Analysis 1: test in-vitro of two molecules for delivering

- responses: 

	- % inhibition of expression of viral protein

- treatments: 

	- molecule: 1 control (Lipofectamine) + 2 candidate molecules (PF6, PF14)

	- serum concentration: percentage (0, 10, 30, 60, 100%)

		- partially crossed: only 0% and 30% available for the three treatments

- only a few combination of treatments have replicates

- proposed models:

	- linear model (ANOVA):

		- y ~ molecule + serum_conc [+ molecule:serum_conc]


### Analysis 2: test in-vivo

- responses:

	- inhibition of luminiscence (in some scale with very large magnitudes)

- treatments: 3 (Control with only molecule of delivery (without antiviral
treatement; control with only antiviral traitement (without delivery); complete
treatement (antiviral treatement + delivering molecule)

- further factors: 

	- luminescent markers: 2 sources, one is sensitive to the antiviral molecule
	while the other is not and serves as a reference

	- day: 10 days of measurement separated by lags of 3 natural days. Only days 1,
	4 and 7 have measurments for both sources of luminescence

- measurements: 

	- 3 lots of 4 mice measured in 2 legs (= 24 measurements of luminiscence for 3
	days for the reference source and for 10 days for the source sensitive to the
	antiviral molecule = 312 observations - 16 NAs = 296 measurements)


- data pre-processing:

	- take logs for rescaling the response (perhaps more meaningful?)

	- since there are many factors affecting the luminiscence, there is great
	variability in the response. They propose to consider the relationship between
	the sensitive source and the reference source. We discussed whether their ratio
	or difference is more appropriate. If we work in the log scale, perhaps the
	difference would be more meaningful since $\log x - \log x_0 = \log (x/x_0)$.

	However, the reference source is injected differently, and may be affected by
	different factors. Furthermore, it has been measured only in days 1, 4 and 7.
	They derived a _standardized_ response dividing by the nearest reference
	measure, but this can introduce more noise.


- modelling:

	- check for mice and manipulator effects

	- either use GAMs to model curves of evolution, or model only days 1, 4 and 7
	discretely

