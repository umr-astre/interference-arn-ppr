
## 2017-10-24 Cécile Minet via e-mail

- "12PPR01_essai 3.xlsx"

  ancien fichier que nous avions utilisés pour nos calculs et à partir duquel
  nous avons discuté l'autre jour


- "topo manip in vivo.xlsx"

  tableau récapitulatif sur l'expérience réalisée in vivo chez la souris avec
  les données brutes


## 2017-11-09 Renata Almeida via e-mail

- "Data tables for PepFect-6-14 2017_RSA.xlsx"

  données __in vitro__ sur les peptides PF6 et PF14.
  
  dans la dernière feuille j'ai compilé toutes les donnée brutes
  qu'il a obtenu (il n'y a malheureusement presque pas de répétitions)
